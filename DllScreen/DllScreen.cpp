// DllScreen.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <d3d11.h>
#include <wrl.h>
#include <dxgi1_6.h>
#include <algorithm>
#include <string>
#include <memory>
#include <vector>

#pragma comment(lib, "d3d11.lib") 

using namespace Microsoft::WRL;

ComPtr<ID3D11Device> device;
ComPtr<IDXGIOutputDuplication> duplication;
ComPtr<ID3D11DeviceContext> context;

//https://gist.github.com/Arnold1/0bec046c0d82c6a78ea13ee7c4f176b3
extern "C" __declspec(dllexport) int initialize() {

	
	ComPtr<IDXGIAdapter> adapter;

	HRESULT hr = D3D11CreateDevice(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE,       // There is no need to create a real hardware device.
		nullptr,
		0,  // Check for the SDK layers.
		nullptr,                    // Any feature level will do.
		0,
		D3D11_SDK_VERSION,
		&device,                    // No need to keep the D3D device reference.
		nullptr,                    // No need to know the feature level.
		&context                     // No need to keep the D3D device context reference.
	);


	ComPtr<IDXGIDevice> dxgiDevice;

	hr = device.As(&dxgiDevice);

	dxgiDevice->GetAdapter(&adapter);

	ComPtr<IDXGIOutput> output;
	ComPtr<IDXGIOutput1> output1;
	

	int i = 0;

	while (adapter->EnumOutputs(i, &output) != DXGI_ERROR_NOT_FOUND) {

		hr = output.As(&output1);

		if (SUCCEEDED(hr)) {

			hr = output1->DuplicateOutput(device.Get(), &duplication);

			if (SUCCEEDED(hr)) {
				break;
			}
			//break;
		}

		++i;

	}


	return 0;
}

extern "C" __declspec(dllexport) char * screenshot(int* len) {

	char * aPFNs = (char *)HeapAlloc(GetProcessHeap(), 0x00000008, 100);

	strcpy_s((char*)(aPFNs), 100, "Robert");

	DXGI_OUTDUPL_DESC dupDesc;

	duplication->GetDesc(&dupDesc);


	D3D11_TEXTURE2D_DESC desc;


	// Create CPU access texture

	desc.Width = dupDesc.ModeDesc.Width;

	desc.Height = dupDesc.ModeDesc.Height;

	desc.Format = dupDesc.ModeDesc.Format;

	desc.ArraySize = 1;

	desc.BindFlags = 0;

	desc.MiscFlags = 0;

	desc.SampleDesc.Count = 1;

	desc.SampleDesc.Quality = 0;

	desc.MipLevels = 1;

	desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE;
	desc.Usage = D3D11_USAGE_STAGING;

	ComPtr<ID3D11Texture2D> lDestImage;

	HRESULT hr = device->CreateTexture2D(&desc, NULL, &lDestImage);

	if (FAILED(hr))
		return false;

	if (lDestImage == nullptr)
		return false;
	DXGI_OUTDUPL_FRAME_INFO frameInfo;
	ComPtr<IDXGIResource> desktopResource;
	ComPtr<ID3D11Texture2D> desktopImage;
	hr = duplication->AcquireNextFrame(1000, &frameInfo, &desktopResource);

	hr = desktopResource.As(&desktopImage);

	// Copy image into CPU access texture
	context->CopyResource(lDestImage.Get(), desktopImage.Get());


	// Copy from CPU access texture to bitmap buffer
	D3D11_MAPPED_SUBRESOURCE resource;
	UINT subResource = D3D11CalcSubresource(0, 0, 0);
	context->Map(lDestImage.Get(), subResource, D3D11_MAP_READ_WRITE, 0, &resource);

	BITMAPINFO	lBmpInfo;

	// BMP 32 bpp
	ZeroMemory(&lBmpInfo, sizeof(BITMAPINFO));

	lBmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);

	lBmpInfo.bmiHeader.biBitCount = 32;

	lBmpInfo.bmiHeader.biCompression = BI_RGB;

	lBmpInfo.bmiHeader.biWidth = dupDesc.ModeDesc.Width;

	lBmpInfo.bmiHeader.biHeight = dupDesc.ModeDesc.Height;

	lBmpInfo.bmiHeader.biPlanes = 1;

	lBmpInfo.bmiHeader.biSizeImage = dupDesc.ModeDesc.Width
		* dupDesc.ModeDesc.Height * 4;


	std::unique_ptr<BYTE> pBuf(new BYTE[lBmpInfo.bmiHeader.biSizeImage]);

	UINT lBmpRowPitch = dupDesc.ModeDesc.Width * 4;

	BYTE* sptr = reinterpret_cast<BYTE*>(resource.pData);
	BYTE* dptr = pBuf.get() + lBmpInfo.bmiHeader.biSizeImage - lBmpRowPitch;

	UINT lRowPitch = std::min<UINT>(lBmpRowPitch, resource.RowPitch);

	for (size_t h = 0; h < dupDesc.ModeDesc.Height; ++h)
	{
		memcpy_s(dptr, lBmpRowPitch, sptr, lRowPitch);
		sptr += resource.RowPitch;
		dptr -= lBmpRowPitch;
	}


	BITMAPFILEHEADER bmpFileHeader;

	bmpFileHeader.bfReserved1 = 0;
	bmpFileHeader.bfReserved2 = 0;
	bmpFileHeader.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + lBmpInfo.bmiHeader.biSizeImage;
	bmpFileHeader.bfType = 'MB';
	bmpFileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	std::vector<BYTE> data;
	data.resize(sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + lBmpInfo.bmiHeader.biSizeImage);

	size_t offset = 0;

	std::memcpy(data.data(), &bmpFileHeader, sizeof(BITMAPFILEHEADER));

	offset += sizeof(BITMAPFILEHEADER);
	std::memcpy(data.data() + offset, &lBmpInfo.bmiHeader, sizeof(BITMAPINFOHEADER));

	offset += sizeof(BITMAPINFOHEADER);
	std::memcpy(data.data() + offset, pBuf.get(), lBmpInfo.bmiHeader.biSizeImage);

	FILE* lfile2 = nullptr;
	auto lerr2 = _wfopen_s(&lfile2, L"temp.bmp", L"wb");

	fwrite(data.data(), data.size(), 1, lfile2);

	fclose(lfile2);

	//HeapFree(GetProcessHeap(), 0x00000008, aPFNs);
	*len = 100;

	return aPFNs;
}